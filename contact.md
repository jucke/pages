---
layout: page
title: Contact
permalink: /contact/
---

Email us at [{{site.email}}](mailto:{{site.email}}) to discuss your next project.

If you want to keep up-to-date with announcements of MLC, you can subscribe to
the MLC-announce mailing list,
[here](https://mailman.zih.tu-dresden.de/groups/listinfo/mlc-announce), you may
send your own announcements there, but the list is strictly moderated. For general
discussions, please use our [zih-mlc](https://mailman.zih.tu-dresden.de/groups/listinfo/zih-mlc) list.

You can find us on [GitLab](https://gitlab.com/mlcdresden). If you wish to share
your machine-learning experiences with the MLC and the world using this site,
you can submit a post through a merge
request [here](https://gitlab.com/mlcdresden/pages/blob/master/README.md).
