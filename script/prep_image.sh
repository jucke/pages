#!/bin/bash
set -e # halt script on error

if [ "$#" -lt 1 ]; then
	echo "Usage prep_image.sh <img> <name>"
fi

IMG="$1"
NAME="$2"
if [ -z "$NAME" ]; then
	NAME=$( echo "$IMG" | sed 's/\(.*\)\..*/\1/')
fi
SUFFIX=$( echo "$IMG" | sed 's/.*\.//')

function checkGen()
{
	if [ ! -z "$1" ]; then
		DST="$NAME"_"$1"."$SUFFIX"
	else
		DST="$NAME"."$SUFFIX"
	fi
	if [ ! -f  "$DST" ]; then
		# no thumbnail given, prepare
		convert -resize "$2" "$IMG" "$DST"
	fi
}

checkGen thumb 535x357

if [ ! -f "$NAME"_thumb@2x."$SUFFIX" ]; then
	convert -resize 50% "$NAME"_thumb."$SUFFIX" "$NAME"_thumb@2x."$SUFFIX"
fi

checkGen lg        1999x1333
checkGen md        991x661
checkGen sm        767x511
checkGen xs        575x383
checkGen placehold 230x153
if [ ! -z "$2" ]; then
	checkGen "" 1920x1080
fi
