BEGIN{
 print "|Time|Presenter|Affiliation|Title|"
 print "|---|---|---|---|"
 FS="\t"
 blockBegin=""
}
{
	if ( match($1, /^[0-9]+:.*[0-9]/ ) ) {
		if ( $6 == "" ) {
			blockBegin = $1
		} else {
			print "|"$1"| | |"$6"|"
		}
	}
	else if ( $1 == "" && $6 != "") {
		if(blockBegin == "") {
			print "| |"$3"|"$4"|"$6"|"
		} else {
			print "|"blockBegin"|"$3"|"$4"|"$6"|"
			blockBegin=""
		}
	}
}
END{

}
